TLSDIR=./dev/tls/

CAFILE = $(TLSDIR)rootCA.pem
CERTFILE = $(TLSDIR)server.crt
CERTKEYFILE = $(TLSDIR)server.key

all:
	@$(MAKE) \
		ssl \
		ssl-root \
		client-start \
		local-dev

ssl:
	@mkcert --key-file $(CERTKEYFILE)  --cert-file $(CERTFILE) "*.test.dev"

ssl-root:
	@cp "$(shell mkcert -CAROOT)"/rootCA.pem  $(CAFILE)

client-start:
	@cd client && npm install
	@cd client && npm start &
	@sleep 3

local-dev:
	@./dev/scripts/localDevEnv.sh