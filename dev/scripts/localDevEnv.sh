#!/bin/bash

# This script starts a Docker container with the required configuration to run a server program in the server directory.
# It sources environment variables from a MHUB's env file, creates a .env file if it doesn't already exist, sets the address
# of the Docker host based on the operating system type, and writes it to the .env file. It then starts the Docker containers
# in the background, and runs the server program in the server directory using the go run command. The script also sets up a
# trap for the SIGINT signal (i.e. Ctrl+C) to shut down Docker containers, remove the ADDR variable from the .env file,
# and exit the script cleanly.

# Set up a trap for the SIGINT signal (i.e. Ctrl+C) to execute the function ctrl_c
trap ctrl_c SIGINT

# Text color
BLUE='\033[36m'
GREEN='\033[1;32m'
ONRED='\033[1;41m'
NOCOLOR='\033[0m'

# Define function ctrl_c to execute when SIGINT signal is received
function ctrl_c() {
    # Stop and remove Docker containers using docker compose down command
    printf "\n$ONRED%s$NOCOLOR\n\n" "---  SHUTTING DOWN DOCKER  ---"
    docker compose down
    printf "\n$GREEN%s$NOCOLOR\n\n" "---    DOCKER SHUT DOWN    ---"

    # Force kill services running on port 3000
    for process in $(lsof -F p -i :3000 | grep ^p | grep -Eo '[0-9]{1,5}'); do kill $process; done
    
    # Force kill services running on port 8000
    for process in $(lsof -F p -i :8000 | grep ^p | grep -Eo '[0-9]{1,5}'); do kill $process; done

    # If ADDR variable is set, remove it from the .env file
    if [[ -n "$ADDR" ]]; then
        # Use grep command to find all lines in .env file that don't match the ADDR variable and write them to .env.temp
        # Then use mv command to rename .env.temp to .env
        grep -v "$ADDR" .env > .env.temp && mv .env.temp .env
    fi

    # Exit script with code 0 (success)
    exit 0
}

# Start Docker containers in the background using docker compose up command
docker compose up -d

# Run the main.go file in the server directory using the go run command
cd server && air