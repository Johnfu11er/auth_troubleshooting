package main

import (
	"fmt"
	"net/http"
	"github.com/gin-gonic/gin"
	"github.com/lestrrat-go/jwx/v2/jwt"
)

const ORIGIN = "https://my-app.test.dev"


func main() {
	router := gin.Default()

	v1 := router.Group("/api")
	{
		v1.GET("/test", defaultFunc)
	}

	router.Run(":8000")
}

func defaultFunc(c *gin.Context) {
	c.Header("Access-Control-Allow-Origin", ORIGIN)
	parseTesting(c.Request)
}

func parseTesting(req *http.Request) {
	fmt.Println("")
	fmt.Println(req)
	fmt.Println("")
	for k, v := range(req.Header) {
		fmt.Println(k, ":", v)
	}
	fmt.Println("")
	JWT, err := jwt.ParseHeader(req.Header, "Authorization", jwt.WithVerify(false))
	if err != nil {
		fmt.Println("Error:", err)
		return
	}
	fmt.Println("\nParsed JWT:", JWT, "\n")
	
	resourceAccess, isPresent := JWT.Get("resource_access")
	if !isPresent {
		fmt.Println("JWT missing resource_access field")
	} else {
		fmt.Println("\nJWT resource_access:", resourceAccess, "\n")
	}

}

