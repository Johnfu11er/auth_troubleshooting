
const URL = "https://my-app.test.dev/api/"

const App = () => {
  
  const handleTestServerConnection = async() => {
    console.log("contacting server...")
    const _ = await fetch(URL + "test", { method: "GET"})
    
  } 
  
  return (
    <>
      <button
        onClick={handleTestServerConnection}
      >
        Test Server Connection
      </button>
    </>
  )
}

export default App