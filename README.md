# Authorization Troubleshooting
- Small application stack for testing and troubleshooting oauth2 configurations
- Requirements
  - Runs on a Mac (docker compose file is using lima endpoints)
  - Node
  - Go
  - mkcert
  - docker compose
  - Rancher Desktop
- Setup
  - Clone this repo
  - Add this entry to the bottom of your `/etc/hosts` file:
    ```
    127.0.0.1 my-app.test.dev
    ```
  - From the root project directory run the command:
    ```
    make all
    ```
  - After the front end, back end, and containers have started, navigate to [https://my-app.test.dev](https://my-app.test.dev)
    - If you see a 500 or 502 error, wait a few seconds and refresh the page; the containers are still starting up.
  - The login at the `My-Realm` Keycloak page is:
    - Username: `user`
    - Password: `password`
  - Once you have logged into [https://my-app.test.dev](https://my-app.test.dev), click the `Test Server Connection` button to trigger the request header console.log statements on the server
  - You should see console.log statements in the same terminal window that you ran the `make all` command
  - If you need to log into the Keycloak instance that the application is authenticating through, go to [https://keycloak.test.dev](https://keycloak.test.dev), and login with the admin credentials:
    - Username: `admin`
    - Password: `password`
  - The Keycloak realm `my-realm` is being used for application authentication